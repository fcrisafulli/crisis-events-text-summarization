from summarizer import Summarizer
import requests as r
from bs4 import BeautifulSoup

from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/BERT_summarize', methods=['POST'])
def summarize(): #(text)
    try:
        model = Summarizer()
        data = request.json
        summary = model(data['text'])
        return jsonify({'summary':summary}), 200
    except Exception as e:
        return jsonify({'error':str(e)}), 400

if __name__ == '__main__':
    app.run(debug=True)

# HOW TO RUN:
# Run this python file in your terminal
# Open the post_cli_tool2.py file and modify the url string to be 'http://127.0.0.1:5000/BERT_summarize'
# In a different terminal, run python post_cli_tool2.py