# Crisis Events Text Summarization

## Index

 - [SQL](#SQL)

## Specification

### SQL

```sql
TABLE event_collections(
    collection_id INTEGER PRIMARY KEY,
    owner_id INTEGER, 
    --stores a json with 3 categories of input data. See Collection Data chapter for more info
    collection_data TEXT, 
    collection_summary TEXT,
    collection_name TEXT
);

DROP TABLE IF EXISTS users;
CREATE TABLE users(
    user_id INTEGER, 
    user_name TEXT,

    -- hashed username and password for authentication. No, this isnt the most secure thing in the world 
    user_hash TEXT
);
```