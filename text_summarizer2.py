from transformers import BertTokenizer, BertLMHeadModel 
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/BERT_summarize_2', methods=['POST'])
def summarize():
    try:
        data = request.json
        tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        model = BertLMHeadModel.from_pretrained('bert-base-uncased')

        inputs = tokenizer('summarize: ' + data['text'], return_tensors='pt', max_length=512, truncation=True)
        outputs = model.generate(input_ids=inputs['input_ids'], max_length=500, min_length=20, length_penalty=2.0, num_beams=4, early_stopping=True)
        summary = tokenizer.decode(outputs[0], skip_special_tokens=True)

        return jsonify({'summary':summary}), 200
    except Exception as e:
        return jsonify({'error':str(e)}), 400

if __name__ == '__main__':
    app.run(debug=True)

# HOW TO RUN:
# Run this python file in your terminal
# Open the post_cli_tool2.py file and modify the url string to be 'http://127.0.0.1:5000/BERT_summarize_2'
# In a different terminal, run python post_cli_tool2.py