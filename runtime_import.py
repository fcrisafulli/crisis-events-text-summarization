import os
import sys
import importlib
from types import ModuleType


def runtime_import(path: str) -> "list[ModuleType]":
    files = os.listdir(path)
    py_files = [f for f in files if f.endswith(".py")]
    print(f"Importing modules from: {path} {py_files}")
    py_files.sort()
    sys.path.append(path)
    modules_found = []
    for py_file in py_files:
        modules_found.append(importlib.import_module(py_file[:-3]))
    return modules_found

if __name__ == "__main__":
    db_implementations = runtime_import("database_implementation/")
    implementation_class = db_implementations[0].IMPLEMENTATION
    db = implementation_class()
    print(type(db))