from flask import Flask, request, abort
from database_api import CrisisEventsDatabase
from runtime_import import runtime_import

app = Flask(__name__)

database:CrisisEventsDatabase = None

#this function is terrible, oh well!
def database_debug_view():
    db_html = ""
    user_db_html = ""
    for collection in database.get_sample_of_collections():
        db_html += f"<br/><code>{collection}</code>"

    for collection in database.get_sample_of_users():
        user_db_html += f"<br/><code>{collection}</code>"

    return f"""
    <html>
    <head></head>
    <body>
        <p>This service uses {database.get_info()} as its database</p>
        <p>Warning: if there are any '<'  or '>' or '/' or '\\' in the database: this will break! </p>
        <code>CollectionID, UserID, CollectionData, CollectionSummary, CollectionName</code>
        {db_html}
        <br/><br/><br/>
        <code>UserID, UserName, Hash</code>
        {user_db_html}
    </body>
    </html>
    """


@app.route('/database_service', methods=['POST','GET'])
def database_service():
    if request.method == 'POST':
        print(request.json)
        result = ''
        try:
            command = request.json["command"]
            if command == "create_collection":
                database.create_collection(0,request.json["data"]["collection_name"])
            elif command == "get_collection":
                result = str(database.get_collection(0,0))
            else:
                print("Invalid command")
                abort(400)
        except KeyError:
            print("Got poorly formatted request")
            abort(400)

        return result, 200
    elif request.method == 'GET':
        return database_debug_view(), 200
    else:
        abort(400)


if __name__ == '__main__':
    database = runtime_import("database_implementation/")[0].IMPLEMENTATION()
    database.initialize()
    database.create_user("test_user","12345")
    database.create_collection(0,"First Collection")
    database.create_collection(0,"Second Collection")
    database.create_collection(0,"Third Collection")
    app.run()