import requests
import sys

# Specify the API URL we want to send our JSON to
url = 'http://127.0.0.1:5000/database_service'
# Specify the appropriate header for the POST request
headers = {'Content-type': 'application/json'}
# Specify the JSON data we want to send
data = '{"command": "get_collection", "data": {"collection_name":"SENT FROM POST title"}}'

response = requests.post(url, headers=headers, data=data)
print(response.text,response.status_code)