DROP TABLE IF EXISTS event_collections;
CREATE TABLE event_collections(
    collection_id INTEGER PRIMARY KEY,
    owner_id INTEGER, 
    collection_data TEXT, 
    collection_summary TEXT,
    collection_name TEXT
);

DROP TABLE IF EXISTS users;
CREATE TABLE users(
    user_id INTEGER, 
    user_name TEXT, 
    user_hash TEXT
);