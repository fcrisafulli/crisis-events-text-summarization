from flask import Flask, request, jsonify
import nltk          
from nltk.corpus import stopwords      
nltk.download('stopwords') 
nltk.download('punkt')                  
from nltk.tokenize import word_tokenize, sent_tokenize

app = Flask(__name__)

@app.route('/nltk_summarize', methods=['POST'])
def summarize():
    try:
        data = request.json
        text = data['text']

        stopWords = set(stopwords.words("english"))
        words = word_tokenize(text)

        freqTable = dict()  
        for word in words:               
            word = word.lower()                 
            if word in stopWords:                 
                continue                  
            if word in freqTable:                       
                freqTable[word] += 1            
            else:          
                freqTable[word] = 1

        sentences = sent_tokenize(text)                 
        sentenceValue = dict()                     

        for sentence in sentences:               
            for word, freq in freqTable.items():              
                if word in sentence.lower():           
                    if word in sentence.lower():                   
                        if sentence in sentenceValue:                                 
                            sentenceValue[sentence] += freq                       
                        else:                       
                            sentenceValue[sentence] = freq                    

        sumValues = 0                        
        for sentence in sentenceValue:              
            sumValues += sentenceValue[sentence] 

        average = int(sumValues / len(sentenceValue))

        summary = ''      
        for sentence in sentences:
            if (sentence in sentenceValue) and (sentenceValue[sentence] > (1.2 * average)):                
                summary += " " + sentence                  
        
        return jsonify({'summary': summary}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 400

if __name__ == '__main__':
    app.run(debug=True)


# HOW TO RUN:
# Run this python file in your terminal
# Open the post_cli_tool2.py file and modify the url string to be 'http://127.0.0.1:5000/nltk_summarize'
# In a different terminal, run python post_cli_tool2.py

# IGNORE BELOW COMMANDS
# Windows Powershell POST request:
# Invoke-RestMethod -Method POST -Uri http://localhost:5000/summarize -ContentType "application/json" -Body '{"text": <input text here>"}'
    
# In Bash:
# curl -X POST -H "Content-Type: application/json" -d '{"text": "<input text here>"}' http://localhost:5000/summarize