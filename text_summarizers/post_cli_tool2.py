import requests

# Specify the API URL we want to send our JSON to
url = 'http://127.0.0.1:5000/T5_summarize'

# Specify the appropriate header for the POST request
headers = {'Content-type': 'application/json'}

# Specify the JSON data we want to send
with open('short_input.txt', 'r', encoding='utf-8') as file:
    text = file.read()
data = '{"text": "' +text+ '"}'

response = requests.post(url, headers=headers, data=data)
print(response.text, response.status_code)