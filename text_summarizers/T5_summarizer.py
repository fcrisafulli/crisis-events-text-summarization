from flask import Flask, request, jsonify
from transformers import AutoTokenizer, AutoModelWithLMHead

app = Flask(__name__)

tokenizer = AutoTokenizer.from_pretrained('t5-base')
model = AutoModelWithLMHead.from_pretrained('t5-base', return_dict=True)

@app.route('/T5_summarize', methods=['POST'])
def summarize():
    try:
        data = request.json
        text = data['text']

        inputs = tokenizer.encode("summarize: " + text, return_tensors='pt', max_length=512, truncation=True)

        summary_ids = model.generate(inputs, max_length=150, min_length=80, length_penalty=5., num_beams=2)
        summary = tokenizer.decode(summary_ids[0])

        return jsonify({'summary': summary}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 400

if __name__ == '__main__':
    app.run(debug=True)


# HOW TO RUN:
# Run this python file in your terminal
# Open the post_cli_tool2.py file and modify the url string to be 'http://127.0.0.1:5000/T5_summarize'
# In a different terminal, run python post_cli_tool2.py

# IGNORE BELOW COMMANDS
# Windows Powershell POST request:
# Invoke-RestMethod -Method POST -Uri http://localhost:5000/summarize -ContentType "application/json" -Body '{"text": <input text here>"}'
    
# In Bash:
# curl -X POST -H "Content-Type: application/json" -d '{"text": "<input text here>"}' http://localhost:5000/summarize