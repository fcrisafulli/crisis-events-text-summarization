import unittest
from unittest.mock import patch
from text_summarizer import app

class BERTSummarizerTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    @patch('text_summarizer.Summarizer')
    def test_summarize(self, mock_summarizer):
        # Mock the Summarizer class
        mock_instance = mock_summarizer.return_value
        mock_instance.return_value = "This is a summary."

        # Define input data
        input_data = {'text': 'This is a test input.'}

        # Send a POST request to the endpoint
        response = self.app.post('/BERT_summarize', json=input_data)

        # Check the response
        self.assertEqual(response.status_code, 200)
        data = response.get_json()
        self.assertIn('summary', data)
        self.assertEqual(data['summary'], "This is a summary.")

    @patch('text_summarizer.Summarizer')
    def test_error_handling(self, mock_summarizer):
        # Mock the Summarizer class to raise an exception
        mock_instance = mock_summarizer.return_value
        mock_instance.side_effect = Exception('An error occurred.')

        # Define input data
        input_data = {'text': 'This is a test input.'}

        # Send a POST request to the endpoint
        response = self.app.post('/BERT_summarize', json=input_data)

        # Check the response
        self.assertEqual(response.status_code, 400)
        data = response.get_json()
        self.assertIn('error', data)
        self.assertEqual(data['error'], 'An error occurred.')

if __name__ == '__main__':
    unittest.main()