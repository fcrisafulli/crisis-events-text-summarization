import unittest
import json
from unittest.mock import patch
from T5_summarizer import app

class T5SummarizerTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    @patch('T5_summarizer.tokenizer')
    @patch('T5_summarizer.model')
    def test_summarize(self, mock_model, mock_tokenizer):
        mock_tokenizer.encode.return_value = 'encoded_text'
        mock_model.generate.return_value = [1]
        input_data = {
            'text': 'This is a test. It is only a test.'
        }
        response = self.app.post('/T5_summarize', json=input_data)
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 200)
        self.assertIn('summary', data)
        self.assertTrue(isinstance(data['summary'], str))
        mock_tokenizer.encode.assert_called_once_with("summarize: This is a test. It is only a test.", return_tensors='pt', max_length=512, truncation=True)
        mock_model.generate.assert_called_once_with('encoded_text', max_length=150, min_length=80, length_penalty=5., num_beams=2)

    def test_invalid_input(self):
        input_data = {
            'incorrect_key': 'This is a test. It is only a test.'
        }
        response = self.app.post('/T5_summarize', json=input_data)
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 400)
        self.assertIn('error', data)

if __name__ == '__main__':
    unittest.main()
