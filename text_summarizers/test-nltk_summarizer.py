import unittest
import json
from nltk_summarizer import app

class NLTKSummarizerTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_summarize(self):
        input_data = {
            'text': 'This is a test. It is only a test.'
        }
        response = self.app.post('/nltk_summarize', json=input_data)
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 200)
        self.assertIn('summary', data)
        self.assertTrue(isinstance(data['summary'], str))

    def test_invalid_input(self):
        input_data = {
            'incorrect_key': 'This is a test. It is only a test.'
        }
        response = self.app.post('/nltk_summarize', json=input_data)
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 400)
        self.assertIn('error', data)

if __name__ == '__main__':
    unittest.main()
