class CrisisEventsDatabase:    
    def initialize(reset: bool) -> bool:
        """Creates the files neccesary for the database to operate"""
        raise "Not Implemented"
    
    def get_next_user_id() -> int:
        raise "Not Implemented"
    
    def get_next_collection_id() -> int:
        raise "Not Implemented"
    
    def create_collection(self,user_id: int, collection_name:str):
        """
        Returns the collection data if the `user_id` matches in the corresponding `collection_id` entry.
        
        Otherwise, it returns `None`
        
        The list returned is in the format:

        """
        raise "Not Implemented"
    
    def create_user(self, username: str, password: str):
        raise "Not Implemented"
    
    def get_collection(self, collection_id:int, user_id: int):
        """
        Returns the collection data if the `user_id` matches in the corresponding `collection_id` entry.
        
        Otherwise, it returns `None`
        
        The list returned is in the format:

        """
        raise "Not Implemented"
    
    def set_collection(self,collection_id:int, user_id: int, collection_json:str, summary:str, name:str) -> bool:
        """
        Returns the collection data if the `authenticated_user_id` matches in the corresponding `collection_id` entry.
        
        Otherwise, it returns `None`
        
        The list returned is in the format:

        """
        raise "Not Implemented"
    
    def get_sample_of_collections(self):
        raise "Not Implemented"
    
    def get_info(self) -> str:
        "Returns data about this database"
        raise "Not Implemented"